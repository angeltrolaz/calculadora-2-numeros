function calcular() {
    var num1 = Number(document.getElementById("num1").value);
    var num2 = Number(document.getElementById("num2").value);
    var resultado = document.getElementById("resultado");

    for (var i = 1; i <= 5; i++) {
        switch (i) {
            case 1:
                resultado.innerHTML += "La suma de " + num1 + " y " + num2 + " es " + (num1 + num2) + "<br>";
                break;
            case 2:
                resultado.innerHTML += "La resta de " + num1 + " y " + num2 + " es " + (num1 - num2) + "<br>";
                break;
            case 3:
                resultado.innerHTML += "La multiplicación de " + num1 + " y " + num2 + " es " + (num1 * num2) + "<br>";
                break;
            case 4:
                resultado.innerHTML += "La división de " + num1 + " y " + num2 + " es " + (num1 / num2) + "<br>";
                break;
            case 5:
                resultado.innerHTML += "El resto de " + num1 + " y " + num2 + " es " + (num1 % num2) + "<br>";
                break;
        }
    }
}